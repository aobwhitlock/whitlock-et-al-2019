for demes in 20
do
    	mkdir demes_$demes
	cd demes_$demes	
	for mig in 0.0002
        do
	    mkdir mig_$mig
	    cd mig_$mig	
		for founder in {0..1}
	        do		          	
			mkdir founder_$founder
			cd founder_$founder
			mkdir sex_0
			cd sex_0
			for cos in 1 1.1 1.3
			do
				mkdir cos_$cos
			    	cd cos_$cos
				pwd
				cp /proj/burchlab/users/cburch/rec_modifier/genotype.py .
				cp /proj/burchlab/users/cburch/rec_modifier/population.py .
				cp /proj/burchlab/users/cburch/rec_modifier/rec_modifier_evolution.py .
				cp /proj/burchlab/users/cburch/demes_$demes/m_$mig/sex_0/founder_$founder/Meta*10000*pickle .
				cp /proj/burchlab/users/cburch/demes_$demes/m_$mig/sex_0/founder_$founder/config.py .
    			echo cost_of_sex = $cos >> config.py
				echo >> config.py crossover_rate = 0.0
				echo >> config.py free_recombination = 0
				echo >> config.py crossover_rate_mutation_rate = 0.001
				bsub -q week -J mod_c$cos python invasion.py
				cd ..
			done
			cd ../..
		done
		cd ..
	done
	cd ..
done

