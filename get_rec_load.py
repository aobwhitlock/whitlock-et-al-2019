import population
import genotype
import numpy as np
import fnmatch
import os

for file in os.listdir('.'): #match to specific files
	if fnmatch.fnmatch(file, 'Metapopulation*10000*'):
		metapop = population.Metapopulation._unpickle_metapopulation(str(file))
    
        
optimal_exp_state = metapop.deme_list[0].optimal_exp_state
outfile = open("rec_load_10000.txt", "w")
header = ["Generation","RecLoad"] 
outfile.write('\t'.join(map(str,header))+'\n')

recload = []
for deme in metapop.deme_list:
    #deme.reproduce_pop_by_recombination_probability()
    deme.get_population_fitness()

    parent_fitness = []
    offspring_fitness = []
    parents1 = deme.choose_next_generation_individuals() #get indices of N genotypes chosen at random in proportion to their fitness
    parents2 = deme.choose_next_generation_individuals() #get indices of N genotypes chosen at random in proportion to their fitness
    for i in range(len(parents1)):
        offspring = deme.organisms[parents1[i]].recombine(deme.organisms[parents2[i]])
        offspring.calculate_fitness(deme.optimal_exp_state)
        offspring_fitness.append(offspring.fitness)
        parent_fitness.append(np.mean([deme.organisms[parents1[i]].fitness,deme.organisms[parents2[i]].fitness]))
    recload.append(np.mean(np.divide(offspring_fitness,parent_fitness))) #get mean rec load within the deme
result = ["10000",str(np.mean(recload))]
outfile.write('\t'.join(map(str,result))+'\n')
	
outfile.close()
            
