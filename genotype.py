"""
genotype.py: implementation of the gene network model described in Siegal & Bergman (2002) Waddington's canalization
    revisited: developmental stability and evolution. PNAS 99: 10528-32.

Contains the Genotype class.
To be used for dict
"""
import copy
import numpy as np
import numpy.random as rnd
import networkx as nx
import random
import math
#import config_linear as config
from config import *
import config 

__author__ = 'Ricardo Azevedo, Christina Burch, Kayla M Peck, Amanda Whitlock'
__version__ = "0.0.1"

class Genotype(object):
    """
    A gene network defined as described in: Wagner (1996) Does evolutionary plasticity evolve? *Evolution* 50: 1008-1023.

    Attributes:
        adj_matrix: adjacency matrix representation of the gene network
        connectivity: connectivity density
        gene_network: matrix representation of the gene network such that columns represent regulators and rows represent
            their targets
        graph: PyGraphviz representation of the gene network
        n_genes: number of genes
        n_interactions: number of (nonzero) interactions in the gene network
        mean_abs_strength: mean absolute strength of interactions (excluding zeros)
        activation_constant: determines the nature of the transition of the gene network between expression states -1 and 1 during development; default value = 100
        development_steps: number of steps during development; default value = 100
        tau: number of iterations used to evaluate whether the gene network is developmentally stable or not; default value = 10
        developmentally_stable_cutoff: value to compare a developed gene_network to in order to determine whether it is stable or not; default value = 0.0001 (following the epsilon parameter in Siegal and Bergman 2002)
        selection_strength: strength of selection used to calculate_fitness; default value = 100
        mutation_rate: mutation rate per genome per generation; default value = 0.1

    """

    def __init__(self, matrix):
        """
        Create Genotype object based on a matrix.  Check that the matrix is square and a numpy.ndarray.  Set default parameters.

        Parameters:
            matrix: matrix representation of the gene network such that columns represent regulators and rows represent
                their targets (type: numpy.ndarray)

        >>> net = Genotype(np.array([[-1.,  0., -1.], [ 0.,  3.,  0.], [ 0.,  2., 1.]]))
        >>> net.gene_network
        array([[-1.,  0., -1.],
               [ 0.,  3.,  0.],
               [ 0.,  2.,  1.]])
        """
        assert type(matrix) is np.ndarray
        assert matrix.shape[0] == matrix.shape[1]
        self.gene_network = matrix
        self.activation_constant = 100
        self.development_steps = 100
        self.tau = 2
        self.developmentally_stable_cutoff = 0.0001
        self.reproduced_sexually = False
        #self.neutral_locus = 0

    @property
    def n_genes(self):
        """
        Number of genes in the network.

        >>> net = Genotype.generate_random_gene_network(10, .35)
        >>> net.n_genes
        10
        """
        return len(self.gene_network.diagonal())

    @property
    def graph(self):
        """NetworkX representation of the gene network."""
        g = nx.DiGraph(data = self.gene_network.transpose())
        return g

    @property
    def interactions(self):
        """
        List of interactions between genes in the form (regulator, target).

        >>> net = Genotype(np.array([[-1.,  0., -1.], [ 0.,  3.,  0.], [ 0.,  2., 1.]]))
        >>> net.interactions
        [(0, 0), (1, 1), (1, 2), (2, 0), (2, 2)]
        """
        return self.graph.edges()

    @property
    def n_interactions(self):
        """
        Number of nonzero interactions between genes.

        >>> net = Genotype.generate_random_gene_network(10, .42)
        >>> net.n_interactions
        42
        """
        return len(self.interactions)

    @property
    def connectivity(self):
        """
        Connectivity density of the gene network.

        >>> net = Genotype.generate_random_gene_network(10, .55)
        >>> net.connectivity
        0.55
        """
        return  self.n_interactions / float(self.n_genes * self.n_genes)

    #    @property
    #    def mean_abs_strength(self):
    #        """Mean absolute strength of interactions (excluding zeros)."""
    #        return np.abs(self.gene_network).sum() / self.n_interactions
    #
    #    @property
    #    def graph(self):
    #        """PyGraphviz representation of the gene network"""
    #        g = pgv.AGraph(directed = True, strict = False)
    #        g.node_attr['fontname'] = 'helvetica'
    #        g.node_attr['fontsize'] = 16
    #        g.node_attr['shape'] = 'circle'
    #        g.node_attr['color'] = 'gray'
    #        g.node_attr['style'] = 'filled'
    #        # add genes
    #        for gene in range(self.n_genes):
    #            g.add_node(gene)
    #        # add interactions
    #        for regulator in range(self.n_genes):
    #            for target in range(self.n_genes):
    #                weight = self.gene_network[target, regulator]
    #                if weight:
    #                    g.add_edge(str(regulator), str(target))
    #                    e = g.get_edge(str(regulator), str(target))
    #                    e.attr['penwidth'] = np.abs(weight) / self.mean_abs_strength
    #                    e.attr['color'] = 'red'
    #                    if weight < 0:
    #                        e.attr['color'] = 'blue'
    #                        e.attr['arrowhead'] = 'tee'
    #        g.layout(prog = 'dot')
    #        return g
    #
    #    def draw_graph(self, filename):
    #        """Draw gene network using graphviz.  Output PNG file."""
    #        self.graph.draw(filename)

    @property
    def connected_components(self):
        """
        Connected components.  List containing lists of genes in each connected component.

        >>> net = Genotype(np.array([[-1.,  0., -1.], [ 0.,  3.,  0.], [ 0.,  0., 1.]]))
        >>> net.connected_components
        [[0, 2], [1]]
        """
        return nx.connected_components(self.graph.to_undirected())

    @property
    def is_connected(self):
        """
        Whether the gene network is connected (type: bool).

        >>> net = Genotype(np.array([[-1.,  0., -1.], [ 0.,  3.,  0.], [ 0.,  0., 1.]]))
        >>> net.is_connected
        False
        """
        return len(self.connected_components) == 1

    @property
    def activation_constant(self):
        '''
        During development, the activation constant determines the nature of the transition of the gene network
        between expression states -1 and 1, from a step-like transition at high values (e.g. 100) to a gradual
        transition at low values (e.g. 1).
        '''
        return self._activation_constant

    @activation_constant.setter
    def activation_constant(self, activation_constant):
        '''
        Set the activation constant and make sure it is non-negative. During development, the activation constant determines
        the nature of the transition of the gene network between expression states -1 and 1, from a step-like transition at
        high values (e.g. 100) to a gradual transition at low values (e.g. 1).
        '''
        assert activation_constant > 0
        self._activation_constant = activation_constant
    
    @property
    def development_steps(self):
        '''
        Number of time steps over which to run develop()
        '''
        return self._development_steps

    @development_steps.setter
    def development_steps(self, development_steps):
        """
        Set number of time steps over which to run develop().
        """
        self._development_steps = development_steps

    @property
    def tau(self):
        '''
        The number of iterations used to evaluate whether the gene network is developmentally stable or not.
        Tau is also the number of iterations used to calculate the mean_expression_pattern which is used in
        calculate_expression_variance() and in calculate_fitness().
        '''
        return self._tau

    @tau.setter
    def tau(self, tau):
        '''
        Set the value of tau, which is the number of iterations used to evaluate whether the gene network
        is developmentally stable or not. It is also the number of iterations used to calculate the mean_expression_pattern
        which is used in calculate_expression_variance() and in calculate_fitness(). Ensure tau is positive and that
        it is less than development_steps.
        '''
        assert tau > 0
        assert tau < self.development_steps
        self._tau = tau

    @property
    def developmentally_stable_cutoff(self):
        '''
        Cutoff used to determine whether a gene network is developmentally stable or not. If network is stable,
        its fitness is calculated using calculate_fitness(). If network is not stable, its fitness is 0.
        '''
        return self._developmentally_stable_cutoff

    @developmentally_stable_cutoff.setter
    def developmentally_stable_cutoff(self, developmentally_stable_cutoff):
        '''
        Set the value of developmentally_stable_cutoff which is used to determine whether a gene network is developmentally stable or not.
        If network is stable, its fitness is calculated using calculate_fitness(). If network is not stable, its fitness is 0.
        '''
        assert developmentally_stable_cutoff > 0
        self._developmentally_stable_cutoff = developmentally_stable_cutoff
        
    def draw_graph(self):
        """
        Draw gene network using matplotlib.

        Note:
            Genes are labelled from 0.  Thick ends touch target genes.
            Self interactions are shown as gray nodes.
        """
        node_cols = ['white'] * self.n_genes
        diag = self.gene_network.diagonal()
        for i in range(self.n_genes):
            if diag[i] != 0:
                node_cols[i] = 'gray'
        nx.draw(self.graph, node_color = node_cols)

    @staticmethod
    def generate_random_gene_network(n_genes, connectivity):
        """
        Generate random genotype with a given n_genes and connectivity.  Sample weights from standard normal distribution.
        Ensure that connectivity is between 0 and 1.

        Note:
            The realized connectivity will be different from the entered value if n_genes * n_genes * connectivity is not
            an integer (see code example below).

        Parameters:
            n_genes: number of genes (type: int)
            connectivity: connectivity density (type: float)

        >>> net = Genotype.generate_random_gene_network(4, .3)
        >>> net.connectivity
        0.25
        """
        assert 0 <= connectivity <= 1
        n_sites = n_genes * n_genes
        n_nonzero_sites = n_sites * connectivity
        flat_matrix = np.zeros(n_sites)
        flat_matrix[0:(n_nonzero_sites)] = rnd.normal(size = n_nonzero_sites)
        rnd.shuffle(flat_matrix)
        return Genotype(np.reshape(flat_matrix, (n_genes, n_genes)))
	
	  
    def assign_properties(self): #note founder is by default set to having reproduced asexually
        self.mutation_rate = mutation_rate
        self.selection_strength = selection_strength
        self.sex_locus = founder_sex_locus
        self.cost_of_sex = cost_of_sex
        self.neutral_locus = [0.0]*config.num_loci
        self.neutral_locus_mutation_rate = neutral_locus_mutation_rate
        self.migration_rate = migration_rate
        self.replicates = replicates
        if self.sex_locus:
            self.reproduced_sexually = True
        else:
            self.reproduced_sexually = False
    
    def assign_invasion_properties(self):
        self.mutation_rate = mutation_rate
        self.selection_strength = selection_strength
        self.cost_of_sex = cost_of_sex
        
    def mutate_interaction_strength(self, regulator, target, new_strength):
        """
        Replace interaction strength from a designated regulator and target gene with a new strength value. The regulator,
        target, and new_strength values are typically chosen within mutate_random().

        Parameters:
            regulator: regulator gene location
            target: target gene location
            new_strength: new interaction strength

        >>> net = Genotype(np.array([[-1.,  0., -1.], [ 0.,  3.,  0.], [ 0.,  0., 1.]]))
        >>> net.mutate_interaction_strength(1, 2, 9.)
        >>> net.gene_network
        array([[-1.,  0., -1.],
               [ 0.,  3.,  0.],
               [ 0.,  9.,  1.]])
        """
        self.gene_network[target, regulator] = new_strength

    def mutate_gene_network(self, n_mutations):
        """
        Mutate a given number (n_mutations) of random interactions. Choose target and regulator gene pairs randomly and
        sample new interaction strengths from standard normal distribution. Ensure that (1) mutations can only affect
        nonzero interactions, and (2) the same interaction cannot be changed twice.

        Parameters:
            n_mutations: number of mutations

        >>> net = Genotype.generate_random_gene_network(4, .3)
        >>> net.mutate_gene_network(5)
        >>> net.connectivity
        0.25
        """
        if n_mutations > self.n_interactions:
            n_mutations = self.n_interactions
        new_strengths = rnd.normal(size = n_mutations)
        mut_interactions = []
        while len(mut_interactions) < n_mutations:
            x = rnd.randint(0, self.n_interactions)
            if not x in mut_interactions:
                mut_interactions.append(x)
        for i in range(n_mutations):
            interaction = self.interactions[mut_interactions[i]]
            self.mutate_interaction_strength(interaction[0], interaction[1], new_strengths[i])

                
    def mutate_neutral_locus(self):
        """
        Mutate the neutral locus according to a poisson process with poisson parameter neutral_locus_mutation_rate. 
        For each mutation, add a random variate from the standard normal, mean 0, standard deviation 1.
        """
        for locus in range(len(self.neutral_locus)):
            n_mut = rnd.poisson(config.neutral_locus_mutation_rate, 1)
            mutations = rnd.normal(0,1,n_mut)
            self.neutral_locus[locus] += np.sum(mutations)
            
    
    def generate_asexual_offspring(self):
        """
        Generate copy of a Genotype, allowing mutations to occur.
        
        Return a new Genotype object.

        >>> net = Genotype.generate_random_gene_network(4, .5)
        >>> net.mutation_rate = 1.2
        >>> daughter_net = net.generate_asexual_offspring()
        >>> daughter_net.connectivity
        0.5
        """
        offspring = copy.deepcopy(self)
        offspring.mutate_gene_network(rnd.poisson(offspring.mutation_rate))
        offspring.mutate_neutral_locus()
        offspring.reproduced_sexually = False
        return offspring
    
    def sexually_reproduce_individual(self,parent2):   
        recombined = Genotype.recombine(self, parent2)
        recombined.mutate_gene_network(rnd.poisson(recombined.mutation_rate))
        recombined.mutate_neutral_locus()
        recombined.reproduced_sexually = True
        return recombined

    def recombine (self, other):
        '''
        Randomly recombines gene network rows of two parent genotypes. Does not mutate.

        >>> rnd.seed(12345)
        >>> parent1 = Genotype.generate_random_gene_network(3, .5)
        >>> parent1.mutation_rate = 0.1
        >>> parent1.gene_network
        array([[-0.20470766,  0.        ,  0.        ],
               [-0.5557303 ,  0.        ,  0.47894334],
               [ 0.        , -0.51943872,  0.        ]])
        >>> parent2 = Genotype.generate_random_gene_network(3, .5)
        >>> parent2.mutation_rate = 0.1
        >>> parent2.gene_network
        array([[ 0.        , -0.76607823,  0.        ],
               [ 0.        , -0.38115577, -1.04306888],
               [ 0.        , -1.57744267,  0.        ]])
        >>> offspring = Genotype.recombine(parent1, parent2)
        >>> offspring.gene_network
        array([[ 0.        , -0.76607823,  0.        ],
               [ 0.        , -0.38115577, -1.04306888],
               [ 0.        , -0.51943872,  0.        ]])
        >>> offspring.mutation_rate
        0.1
        '''
        offspring = copy.deepcopy(self)
        offspring.fitness = []
        offspring.expression_pattern = []
        parent_for_each_locus = rnd.randint(low=0, high=2, size=self.n_genes + 2)
        neut_loci = rnd.randint(low=0, high=2, size=len(self.neutral_locus))
        for locus in range(self.n_genes):
            if parent_for_each_locus[locus]:
                offspring.gene_network[locus] = other.gene_network[locus]
        if parent_for_each_locus[self.n_genes]:               #mutation rate segregates independently
            offspring.mutation_rate = other.mutation_rate
        if parent_for_each_locus[self.n_genes+1]:
            offspring.sex_locus = other.sex_locus
        for neut in range(len(self.neutral_locus)):
            if neut_loci[neut]:
                offspring.neutral_locus[neut] = other.neutral_locus[neut]
        return offspring
        
    def generate_random_initial_expression_state(self):
        '''
        Generate a random initial_expression_state: an array of size n_genes, filled randomly with 1 or -1. Used during
        develop() and also used as the ancestral expression state in calculate_fitness().
        '''
        self.initial_expression_state = np.round(rnd.random(self.n_genes))*2 - 1

    @staticmethod
    def sigmoidal_filter(activation_constant, current_expression_state):
        '''
        Calculate the expression level (which will be between 1 and -1) of a gene_network during development (see develop())
        by filtering the current_expression_state through the sigmoidal_filter_function: f(x) = 2/(1+e^-ax) - 1
        following Siegal and Bergman 2002. The method returns an array of length n_genes which will then be multiplied
        by the gene_network, resulting in the current_expression_state which will again be filtered through the
        sigmoidal_filter(). This is repeated for development_steps number of times.

        Parameters:
            activation_constant: activation_constant of gene network that controls transition between 1 and -1
            current_expression_state: current expression during development
        '''
        if np.any(current_expression_state < -7):
            i = 0
            for i in range(0,len(current_expression_state)):
                if current_expression_state[i] < -7:
                    current_expression_state[i] = -7
        return (2/(1+np.exp(-activation_constant*current_expression_state)) - 1)

    def develop(self):
        '''
        Simulate development: multiply the gene_network (matrix: n_genes x n_genes) by the initial_expression_state (array: n_genes)
        and pass the resulting current_expression_state (array: n_genes) through the sigmoidal_filter().
        The resulting array of n_genes (appended to the expression_pattern) is then multiplied by the gene_network to generate
        the current_expression_state for the next time step. This is repeated development_steps number of times. The output is a
        list of the current_expression_state arrays from each development_step time step (where each array is
        of length n_genes).
        '''
        self.expression_pattern = []
        self.expression_pattern.append(self.initial_expression_state)
        for step in range(0, self.development_steps):
            current_expression_state = np.dot(self.gene_network, self.expression_pattern[step])
            #self.expression_pattern.append(Genotype.sigmoidal_filter(self.activation_constant,current_expression_state))
            self.expression_pattern.append(np.sign(current_expression_state))
            if self.expression_pattern[step].all == self.expression_pattern[step -1].all:
                self.path_length = step
                break
            else:
                self.path_length = "N/A"
        interval_expression_pattern = self.expression_pattern[(self.development_steps-self.tau):self.development_steps]
        self.mean_expression_pattern = np.mean(interval_expression_pattern, axis=0)

    def develop_new(self):
        '''

        Simulate development: multiply the gene_network (matrix: n_genes x n_genes) by the initial_expression_state (array: n_genes)

        and pass the resulting current_expression_state (array: n_genes) through the sigmoidal_filter().

        The resulting array of n_genes (appended to the expression_pattern) is then multiplied by the gene_network to generate

        the current_expression_state for the next time step. This is repeated development_steps number of times. The output is a

        list of the current_expression_state arrays from each development_step time step (where each array is

        of length n_genes).

        '''
        self.expression_pattern = []
        self.expression_pattern.append(self.initial_expression_state)
        for step in range(0, self.development_steps):
            current_expression_state = np.dot(self.gene_network, self.expression_pattern[step])
            #self.expression_pattern.append(Genotype.sigmoidal_filter(self.activation_constant,current_expression_state))
            self.expression_pattern.append(np.sign(current_expression_state))
            if self.expression_pattern[step].all == self.expression_pattern[step -1].all:
                self.path_length = step
                break
            else:
                self.path_length = "N/A"
        self.mean_expression_pattern = current_expression_state

    @staticmethod
    def calculate_difference_from_specified_expression(specified_expression_pattern,expression_state,n_genes):
        '''
        Calculate the sum of the squared difference between a specified expression pattern and an individual expression state from
        the gene network, which is then divided by 4*n_genes. Used to determine whether a gene_network is developmentally
        stable (see calculate_expression_variance()) and used to calculate_fitness(). Output is a single value.
        '''
        assert len(specified_expression_pattern) == len(expression_state)
        difference_from_specified_expression = np.sum(np.subtract(specified_expression_pattern, expression_state)**2)/(4*n_genes)
        return difference_from_specified_expression

    def calculate_expression_variance(self):
        '''
        Calculate how much the last tau expression states of the expression_pattern vary from the mean_expression_pattern.
        Take each expression state from (development_steps - tau) to development_steps and calculate_difference_from_specified_expression where the
        specified_expression_pattern is the mean_expression_pattern. Sum the differences over the last tau steps and the
        output is a single value that will be compared to the developmentally_stable_cutoff value in calculate_fitness().
        '''
        equilibrium_steady_state = []
        for i in range ((self.development_steps - self.tau),self.development_steps):
            equilibrium_steady_state.append(Genotype.calculate_difference_from_specified_expression(self.mean_expression_pattern, self.expression_pattern[i], self.n_genes))
        return np.sum(equilibrium_steady_state)

    def is_developmentally_stable(self):
        '''
        Check whether the gene network is developmentally stable or not (type: bool)
        '''
        return self.calculate_expression_variance() < self.developmentally_stable_cutoff

    def calculate_fitness(self, optimal_expression_state):
        '''
        Calculate the fitness of a gene_network. If the gene_network is not developmentally stable after going through development
        (develop()), it has a fitness of 0. If the gene_network is developmentally stable, its fitness is calculated using 1)
        calculate_difference_from_specified_expression where the difference between the mean_expression_state and the
        ancestral expression state which is the final expression state of the founder individual
        (founder.expression_pattern[founder.development_steps]), and 2) the selection_strength (following Siegal and Bergman 2002).
        Output is a single value between 0 and 1.
        '''
        self.develop() #calculates new expression stat       
        if self.is_developmentally_stable():
            difference_from_optimal_expression = Genotype.calculate_difference_from_specified_expression(optimal_expression_state, self.mean_expression_pattern, self.n_genes)
            #1 compare to threshold (create new attribute epsilon ~ 10^-4), if less, then stable
            #2 if not stable then fitness = 0
            #3 if stable, then calculate difference_from_specified_expression
            #in 2006 paper, this was the ancestor's stable expression state
            #feed this difference in to below equation as D
            self.fitness = np.exp(-difference_from_optimal_expression/self.selection_strength)
            if self.reproduced_sexually:
                self.fitness = self.fitness/self.cost_of_sex
        else:
            self.fitness = 0
            
    def fitness_new(optimum):
         if self.path_length <= self.development_steps-2:
            difference_from_optimal_expression = Genotype.calculate_difference_from_specified_expression(optimal_expression_state, self.mean_expression_pattern, self.n_genes)
            self.fitness = np.exp(-difference_from_optimal_expression/self.selection_strength)
         else:
            self.fitness = 0
        
    def calculate_mutant_fitness_distribution(self, optimal_expression_state):
        """Generate data needed to calculate mutation effect distribution. For each replicate, mutate a gene_network and measure its fitness.Output is an array of
         the resulting fitness values and summary statistics to describe the distribution.
        """
        fitness_array = []
        self.multiple_mutation_robustness = [] #make this a list so, even though will be single value so is compatible with methods that use calculate_robustness()
        for rep in range(0,self.replicates):
            temporary_network = copy.deepcopy(self)
            temporary_network.mutate_gene_network(1)
            temporary_network.develop()
            temporary_network.calculate_fitness(optimal_expression_state)
            fitness_array.append(temporary_network.fitness)
            
        self.mutant_fitness_distribution = fitness_array
        self.multiple_mutation_robustness.append(np.mean(fitness_array, axis=0).tolist())
        self.mean_mutant_fitness=np.mean(fitness_array, axis=0)
        self.frequency_beneficial = float(np.sum(np.array(fitness_array)>self.fitness))/self.replicates
        self.frequency_neutral = float(np.sum(np.array(fitness_array)==self.fitness))/self.replicates
        if self.fitness == 0:
            self.frequency_lethal = 0
            self.frequency_deleterious = 0
        else:
            self.frequency_lethal = float(np.sum(np.array(fitness_array)==0.0))/self.replicates
            self.frequency_deleterious = float(np.sum(np.array(fitness_array)<self.fitness))/self.replicates - self.frequency_lethal
    
    def calculate_robustness(self, optimal_expression_state):
        """
        Generate data needed to calculate epistasis in R. For each replicate, mutate a gene_network and determine if it is
        stable or not. Continue up to number_of_mutations. Output is an array of the number of stable gene_networks for
        1:number_of_mutations.
        """
        fitness_array = []
        if not self.fitness:
            self.multiple_mutation_robustness = np.zeros(config.number_of_mutations).tolist()
        else:
            for rep in range(config.replicates):
                temporary_network = copy.deepcopy(self)
                temporary_fitness_array = [0]*config.number_of_mutations
                
                for mut in range(config.number_of_mutations):
                    temporary_network.mutate_gene_network(1)
                    temporary_network.develop()
                    temporary_network.calculate_fitness(optimal_expression_state)
                    temporary_fitness_array[mut] = temporary_network.fitness
                fitness_array.append(temporary_fitness_array)
            self.multiple_mutation_robustness = np.mean(fitness_array, axis=0).tolist()
    
    def calc_genetic_variation(self, other):
        distance = 0
        for i in xrange(self.n_genes):
            for j in xrange(self.n_genes):
                int1 = self.gene_network[i][j]
                int2 = other.gene_network[i][j]
                if int1 != int2:
                    distance += 1.0 
        het = distance/(self.connectivity*self.n_genes**2)
        return het

    @staticmethod
    def calc_phenotypic_distance(genotype1, genotype2, n_genes):
        distance = 0
        for i in range(n_genes):
            exp1 = genotype1.mean_expression_pattern[i]
            exp2 = genotype2.mean_expression_pattern[i]
            if exp1 != exp2:
                distance += 1.0
        prop_dist = distance/n_genes
        return prop_dist
        
    def change_org_exp_state(self, new_opt):
        self.mean_expression_pattern = new_opt
        self.initial_expression_state = new_opt 
        self.calculate_fitness(new_opt)
        
class Linear_genotype(object):
    def __init__(self): 
        self.genome = [1.0]*config.length
    
    @staticmethod
    def create_linear_genotype():
        genotype = Linear_genotype()
        genotype.genome = [0]*config.length
        genotype.mutation_rate = config.mutation_rate
        genotype.cost_of_sex = config.cost_of_sex
        genotype.sex_locus = config.sex_locus
        genotype.del_rate = config.del_rate
        genotype.del_val = config.del_val
        genotype.ben_rate = config.ben_rate
        genotype.ben_val = config.ben_val
        genotype.crossover_prob = config.crossover_prob
        return genotype
        
        
    def mutate_linear(self):
        num_ben = rnd.poisson(self.ben_rate*self.mutation_rate * len(self.genome))
        num_del = rnd.poisson(self.mutation_rate * self.del_rate * len(self.genome))
        total_mut = num_ben + num_del
        temp_mut = 0
        mut_interactions = []
        while len(mut_interactions) < total_mut:
            rand_site = rnd.randint(0,len(self.genome))
            if not rand_site in mut_interactions:
                mut_interactions.append(rand_site)
                #temp_mut += 1
        for locus in range(len(mut_interactions)):
            self.genome[mut_interactions[locus]] = (self.del_val if locus < num_del else self.ben_val)

    def recombine(self,parent2):                        
        num_crossovers = rnd.poisson(self.crossover_prob*len(self.genome))
        #num_crossovers = 2
        offspring = copy.deepcopy(self)
        offspring.genome = []
        crossover_vect = sorted(random.sample(range(1,len(self.genome)), num_crossovers))
        #or pick rand num {0..1} and if < 0.5, use parent1 or w/e
        parents = [self, parent2]
        random.shuffle(parents)
        current_parent = parents[0]
        start_val = 0
        for i in range(len(current_parent.genome)):
            if len(crossover_vect) > 0 and i == crossover_vect[0]:
                start_val += 1
                current_parent = parents[start_val%2]
                del(crossover_vect[0])
            offspring.genome.append(current_parent.genome[i])
        return offspring
        
    def get_linear_rec_load(self):
        sex_orgs, asex_orgs = self.choose_mixed_all_alive()
        for orgs in (sex_orgs, asex_orgs):
            if len(orgs) < 1: #skip this method if there ar eno living organisms
                next
            else:
                old_orgs = []
                new_orgs = []
                orgs2 = sorted(orgs, key=lambda k: random.random())
            for parent in len(xrange(orgs)):
                old_orgs.append((orgs[parent].fitness + orgs2[parent].fitness)/2) 
                new_orgs.append(orgs[parent].recombine(orgs2[parent]).get_fitness())
        recload = np.mean(np.divide(new_orgs,old_orgs)) #get mean fitness of new orgs relative to old orgs
        return recload            
    #def new_rec(seq1, seq2, loc):
        #return seq1[:loc] + seq2[loc:]
        #sort a list of recombination loci
        #then make the string for each segment between those                       
                   
        
    def get_fitness(self):
        num_del = len([locus for locus in self.genome if locus == self.del_val])
        num_ben = len([locus for locus in self.genome if locus == self.ben_val])
        self.fitness = (math.pow(self.ben_val, num_ben)*math.pow(self.del_val, num_del))   

        
    def generate_asexual_offspring(self):
        offspring = copy.deepcopy(self)
        offspring.mutate_linear()
        return offspring
 
    def sexually_reproduce_individual(self,parent2):   
        recombined = self.recombine(parent2)
        recombined.mutate_linear()
        return recombined     
              

if __name__ == "__main__":
    import doctest
    doctest.testmod()
