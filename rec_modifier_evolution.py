# -*- coding: utf-8 -*-
import population
import os
import genotype
import config
import fnmatch
import copy
import random
import numpy as np

#set parameters from config file
pop_size = config.pop_size
n_demes = config.n_demes
n_migrants = config.n_migrants
foundernum = config.foundernum
cost = config.cost_of_sex 
crossover_rate = config.crossover_rate
crossover_rate_mutation_rate = config.crossover_rate_mutation_rate
free_recombination = config.free_recombination

#open file for data storage and write header
pid = os.getpid()
population_datafile = open("rec_modifier_0.1_evolution_data_cost_"+str(cost)+"_founder_"+str(foundernum)+"_"+str(pid)+".txt","a")
population_datafile.write("Founder"+"\t"+"\t"+"Gen"+"\t"+"Crossover Rate"+"\n")
population_datafile.close()

#open pickled pop
for file in os.listdir('.'): #match to specific files
    if fnmatch.fnmatch(file, 'Metapopulation*'):
        filename = str(file)
        generation = filename.split('_')[3]
        #print(filename)
        print(generation)
        if generation == "10000":        
            print(filename)
            metapop1 = population.Metapopulation._unpickle_metapopulation(filename)
            optimum = metapop1.deme_list[0].optimal_exp_state
            print(optimum)

            for deme in metapop1.deme_list:
                deme.get_population_fitness()
				for org in deme.organisms:
					org.assign_properties()
					org.assign_chromosome_properties(org.n_genes,1)

            #continue evolution, now allowing recurrent mutation at crossover_rate locus
            metapop = copy.deepcopy(metapop1)
            #begin evolution
            current_gen_num = 10001
            for gen in range(current_gen_num,30001):
                metapop.migrate(n_migrants)
                mean_crossover_rate = []
                for i in xrange(config.n_demes):
                    metapop.deme_list[i].reproduce_pop_by_crossover_probability()
                    metapop.deme_list[i].get_population_fitness()
                if gen%10==0:
                    mean_crossover_rate.append(np.mean([org.crossover_rate for org in metapop.deme_list[i].organisms]))
                    population_datafile = open("rec_modifier_0.1_evolution_data_cost_"+str(cost)+"_founder_"+str(foundernum)+"_"+str(pid)+".txt","a")
                    population_datafile.write(str(foundernum) + "\t" + str(gen) + "\t" + str(np.mean(mean_crossover_rate)) + "\n")
                    population_datafile.close()
                if gen%10000==0:
                    metapop._pickle_metapopulation(config.foundernum, gen, pid, 0)
            
