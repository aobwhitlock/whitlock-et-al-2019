# -*- coding: utf-8 -*-
import population
import genotype
import config
import os

#use process id as unique run identifier
pid = os.getpid()

#unpickle founder and assign all its properties
founding_pop = population.Matrix_population._unpickle(3)
founder = founding_pop.organisms[config.foundernum]
founder.assign_properties()
founder.develop()
optimum = founder.mean_expression_pattern
founder.calculate_fitness(optimum)

#create demes and metapopulation
metapop = population.Metapopulation_matrix()
metapop.create_demes_from_founder(founder)

#open output files
all_files = population.Metapopulation.open_files(pid)

#begin evolution
for current_gen_num in xrange(0,config.num_generations+1):
    if current_gen_num in config.recording_generations:
        metapop.write_all_data(all_files, optimum, current_gen_num)
    for i in xrange(config.n_demes): 
        #uncomment only one of the next 3 lines to specify Reproductive Mode
        metapop.deme_list[i].reproduce_pop_by_recombination_probability() #separate sex
        #metapop.deme_list[i].reproduce_pop_by_dominant_recombination_probability() #dominant sex
        #metapop.deme_list[i].reproduce_pop_by_recessive_recombination_probability() #recessive sex
        
        metapop.deme_list[i].get_population_fitness()
    metapop.migrate(config.n_migrants)
    if current_gen_num%10000==0:
        metapop._pickle_metapopulation(config.foundernum, current_gen_num, pid, 0)

#end program
population.Metapopulation.close_files(all_files)
