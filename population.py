import genotype
import copy
import numpy as np
import numpy.random as rnd
import cPickle
import random
import pickle
import math
from datetime import datetime
import os
import config
import fnmatch

class Population(object):

    """
    A population of gene networks
    Attributes: population_size
    """
    def __init__(self): 
        self.organisms = []
        self.left_migrants = []
        self.right_migrants = []
        self.n_demes = config.n_demes


    @property
    def population_size(self):
        """
        Number of individuals in the population.
        """
        return self._population_size

    @population_size.setter
    def population_size(self,population_size):
        """
        Set number of individuals in the population.
        Must be greater than zero.
        """
        assert population_size > 0
        self._population_size = population_size
        
    def read_population_fitness(self): #this is only to be used in reading them after migration, before reproduction
        self.pop_fitness_values = []
        for org in self.organisms:
            self.pop_fitness_values.append(org.fitness)
    
    def set_proportion_sexual (self, proportion):
        """
        Create a population with both sexual and asexual members by entering the proportion that should be sexual which will recombine with   
        probability of 1. total organisms - %sexual will reproduce with prob of 0. This is for use in the founding population.
        In population of size 2:
        >>>founding_population.set_proportion_sexual(0.5)
            founding_population.organisms[0].sex_locus = 1
            founding_population.organisms[0].sex_locus = 0
        """
        assert 0 <= proportion <=1
        #self.proportion_sexual = proportion
        number_sexual = int(self.population_size*proportion)
        indices = rnd.permutation(range(self.population_size))
        for i in xrange(0, number_sexual):
            self.organisms[indices[i]].sex_locus = 1
        for i in xrange(number_sexual, self.population_size):
            self.organisms[indices[i]].sex_locus = 0
        self.proportion_sex = proportion
    
    def set_proportion_asexual (self, proportion):
        """
        Create a population with both sexual and asexual members by entering the proportion that should be sexual which will recombine with   
        probability of 1. total organisms - %sexual will reproduce with prob of 0. This is for use in the founding population.
        In population of size 2:
        >>>founding_population.set_proportion_sexual(0.5)
        founding_population.organisms[0].sex_locus = 1
        founding_population.organisms[0].sex_locus = 0
        """
        assert 0 <= proportion <=1
        self.proportion_sexual = 1 - proportion
        number_asexual = int(self.population_size*proportion)
        indices = rnd.permutation(range(self.population_size))
        for i in xrange(0, number_asexual):
            self.organisms[indices[i]].sex_locus = 0
        for i in xrange(number_asexual, self.population_size):
            self.organisms[indices[i]].sex_locus = 1
    
    
    def make_a_deme_asexual(self):
        for i in xrange(len(self.organisms)):
            self.organisms[i].sex_locus = 0    
        
    def get_proportion_sexual (self):
        """
        Calculate the proportion of organisms that are sexual (based on flag set at time of last reproduction in case of invasion)
        """    
        if self.organisms:
            self.proportion_sexual = np.mean([org.sex_locus for org in self.organisms])
        else:
            self.proportion_sexual = None
    
    def choose_all_alive(self): #returns orgs with fitness greater than 0, ie living orgs
        indices = [org for org in self.organisms if org.fitness > 0]
        return indices
        
                            
    def choose_next_generation_individuals(self): 
        """
        Generates a sample of parents for the next generation, randomly, with replacement, weighted by fitness
        such that an organism with fitness of 1 is twice as likely to be selected as one with fitness of 0.5.
        Returns a list indices that correspond to the population_size sampled parents.
        """
        if self.organisms: #if there are organisms in the population
            self.read_population_fitness()
            cum_fitness = np.cumsum(self.pop_fitness_values) #creates a summed fitness vector
            rand_array = rnd.random_sample(self.population_size) #chooses random points within this vector 
            mult_rand_array = np.multiply(rand_array, cum_fitness[len(self.organisms)-1])
            indices = np.searchsorted(cum_fitness, mult_rand_array) #gets indices of genotypes which correspond to rand choices
        else:
            indices = []
        return indices


    def reproduce_pop_by_dominant_recombination_probability (self): #is run for each deme
        """
        Reproduction of the population.
        ith elements of chosen_genotypes1 and chosen_gentoypes2 represent pairs of potential parents
        Each pair reproduces sexually or asexually according to their genotypes at sex_locus: 1 = sexual, 0 = asexual         .
        """
        new_orgs = []
        chosen_genotypes1 = Population.choose_next_generation_individuals(self) #get N genotypes
        chosen_genotypes2 = Population.choose_next_generation_individuals(self) #get N genotypes
        assert len(chosen_genotypes1) > 0 #if there are no organisms in the population give error
        for i in range(len(chosen_genotypes1)):
            if (self.organisms[chosen_genotypes1[i]].sex_locus == 0) & (self.organisms[chosen_genotypes2[i]].sex_locus == 0): #if both chosen genotypes are asexual, first gets to reproduce
                offspring = self.organisms[chosen_genotypes1[i]].generate_asexual_offspring()
            else: #if one or both are sexual, reproduce sexually  
                offspring = self.organisms[chosen_genotypes1[i]].sexually_reproduce_individual(self.organisms[chosen_genotypes2[i]]) 
            new_orgs.append(offspring)
        self.organisms = new_orgs
        

    def reproduce_pop_by_recessive_recombination_probability (self): #is run for each deme
        """
        Reproduction of the population.
        ith elements of chosen_genotypes1 and chosen_gentoypes2 represent pairs of potential parents
        Each pair reproduces sexually or asexually according to their genotypes at sex_locus: 1 = sexual, 0 = asexual         .
        """
        new_orgs = []
        chosen_genotypes1 = Population.choose_next_generation_individuals(self) #get N genotypes
        chosen_genotypes2 = Population.choose_next_generation_individuals(self) #get N genotypes
        assert len(chosen_genotypes1) > 0 #if there are no organisms in the population give error
        for i in range(len(chosen_genotypes1)):
            if (self.organisms[chosen_genotypes1[i]].sex_locus == 1) & (self.organisms[chosen_genotypes2[i]].sex_locus == 1): #if both chosen genotypes are sexual, reproduce sexually
                offspring = self.organisms[chosen_genotypes1[i]].sexually_reproduce_individual(self.organisms[chosen_genotypes2[i]])
            else: #if one or both are asexual, clone the first genotype 
                offspring = self.organisms[chosen_genotypes1[i]].generate_asexual_offspring()
            new_orgs.append(offspring)
        self.organisms = new_orgs      
    
                                        
    def reproduce_pop_by_recombination_probability (self): #is run for each deme
        """
        Reproduction of the population.
        ith elements of chosen_genotypes1 and chosen_gentoypes2 represent pairs of potential parents
        Each pair reproduces sexually or asexually according to their genotypes at sex_locus: 1 = sexual, 0 = asexual         .
        """
        if not len(self.organisms): #go on to next deme if there are no organisms in the population. is not an error--it sometimes occurs if deme size is small and mig rate is high
            return
        new_orgs = []
        chosen_genotypes1 = Population.choose_next_generation_individuals(self) #get N genotypes 
        new_population = Matrix_population() #make them a new population so the same population methods can be used for them
        new_population.population_size = self.population_size
        new_population.organisms = [i for i in self.organisms if i.sex_locus == 1] #populate the population
        chosen_genotypes2 = new_population.choose_next_generation_individuals() #these indices correspond to new_population
        for i in chosen_genotypes1:
            if self.organisms[i].sex_locus: #if chosen genotype is sexual, mate with org from sexual pop
                offspring = self.organisms[i].sexually_reproduce_individual(new_population.organisms[chosen_genotypes2[0]]) #take first new parent from list
                chosen_genotypes2 = np.delete(chosen_genotypes2, 0) #making new array to remove parent that has reproduced bc numpy matrix is immutable
            else: #if chosen genotype is asexual, clone it  
                offspring = self.organisms[i].generate_asexual_offspring()
            new_orgs.append(offspring)
        self.organisms = new_orgs

          
    def reproduce_mixed_pop(self, n_sexual): #this passes sexual organisms to choose_next_generation_sexuals
        new_population = Matrix_population() #make them a new population so the same population methods can be used for them
        new_population.organisms = [i for i in self.organisms if i.sex_locus == 1] #populate the population
        chosen_sexuals = new_population.choose_next_generation_sexuals(n_sexual) #sexual parents chosen in 1st choose_genotypes, so no extra will be calculated
        return chosen_sexuals #do these indices correspond to the position in new_population when you want them to correspond to self?          
     
    def _pickle(self, generation_number):
        file = open("Population_"+str(generation_number)+".pickle", "w")
        pickle.dump(self, file)
        file.close()

    @staticmethod
    def _unpickle(pop_id):
        file = open("Population_"+str(pop_id)+".pickle", "r")
        pop = pickle.load(file)
        file.close()
        return pop


####################################################################### MATRIX #methods that only apply to gene network populations
class Matrix_population(Population):
        

    @staticmethod
    def found_clonal_matrix_population(founder): #gets all properties from config file. these are not global variables, must be called with syntax config.whatever_variable
        """
        Make a population by copying a founder genotype into a list population_size times.

        >>> founder = population.Population.generate_founder(5,.2,1)
        >>> g0 = population.Population.found_clonal_population(founder,10)
        >>> g0
        <population.Population at 0x344fb30>
        >>> g0.organisms[2].gene_network
        array([[ 0.48238438,  0.        ,  0.        ,  0.08485553,  0.49141507],
        [-0.79636121,  0.        ,  0.        ,  0.        ,  0.        ],
        [ 0.        ,  0.        ,  0.        ,  0.        ,  0.        ],
        [ 0.        ,  0.        ,  0.        ,  0.        , -0.88783723],
        [ 0.        ,  0.        ,  0.        ,  0.        ,  0.        ]])
        """
        new_population = Matrix_population()
        new_population.optimal_exp_state = founder.mean_expression_pattern
        new_population.replicates = config.replicates
        new_population.number_of_mutations = config.number_of_mutations
        new_population.n_deme_draws = config.n_deme_draws
        new_population.n_meta_draws = config.n_meta_draws
        new_population.population_size = config.pop_size
        while len(new_population.organisms) < new_population.population_size: #keep adding founder clones til the population has reached designates size
            new_population.organisms.append(copy.deepcopy(founder))
        new_population.pop_fitness_values = [founder.fitness]*new_population.population_size
        return new_population


    @staticmethod
    def generate_founder(): #not used if unpickling standardized founder file
        """
        Generate a founder consisting of a gene network with random interaction strengths and a mutation rate.

        >>> founder = population.Population.generate_founder(5,.2)
        >>> founder.gene_network
        array([[ 0.48238438,  0.        ,  0.        ,  0.08485553,  0.49141507],
        [-0.79636121,  0.        ,  0.        ,  0.        ,  0.        ],
        [ 0.        ,  0.        ,  0.        ,  0.        ,  0.        ],
        [ 0.        ,  0.        ,  0.        ,  0.        , -0.88783723],
        [ 0.        ,  0.        ,  0.        ,  0.        ,  0.        ]])
        """
        fitness = 0
        while fitness == 0:
            founder = genotype.Genotype.generate_random_gene_network(config.n_genes, config.connectivity)
            founder.generate_random_initial_expression_state()
            founder.mutation_rate = config.mutation_rate
            founder.selection_strength = config.selection_strength
            founder.sex_locus = config.founder_sex_locus
            founder.develop()
            founder.cost_of_sex = config.cost_of_sex
            founder.calculate_fitness(founder.mean_expression_pattern)
            fitness = founder.fitness
        return founder


    def calculate_population_robustness(self, optimal_expression_state):
        """
        Calculate robustness for all individuals in a population. Robustness of an individual gene_network is calculated as the
        mean fitness of number of replicates 1-mutant neighbors (calculate_robustness() in genotype.py).
        Output is an array of the robustness value for all individuals in the population.
        """
        self.population_robustness_values = []
        for i in range(0,self.pop_size):
            self.organisms[i].calculate_robustness(optimal_expression_state, config.replicates, config.number_of_mutations)
            self.population_robustness_values.append(self.organisms[i].robustness)
    
    def calc_pop_gen_var(self): #calculates pairwise distance between orgs within demes
        if len(self.organisms):
            het_sum = 0
            for i in xrange(config.n_deme_draws):
                #rand_vals = rnd.randint(len(self.organisms), size = 2*config.n_deme_draws)
                #for i in xrange(0,2*config.n_deme_draws,2):
                #    org1 = self.organisms[rand_vals[i]]
                #    org2 = self.organisms[rand_vals[i+1]] 
                org1 = random.choice(self.organisms)
                org2 = random.choice(self.organisms)
                while org1 == org2:
                    org2 = random.choice(self.organisms)
                het_sum += org1.calc_genetic_variation(org2)
                het_avg = het_sum/config.n_deme_draws
        else:
            het_avg = "NA"
        return het_avg


    def get_population_fitness(self): #opt_exp_state = founder.mean_expression_pattern
        """
        Calculate fitness of each genotype, create a vector of each genotype's fitness in the population and make it an attribute of the 
        population
        >>> my_example_population.get_population_fitness()
        >>> my_example_population.pop_fitness_values
           [0.9819314116493095,
           0.9757976889184076]
        """
        self.pop_fitness_values = [] #creates a population instance attribute that is an empty list
        for i in range(0, len(self.organisms)):
            self.organisms[i].develop() #develops each organism
            self.organisms[i].calculate_fitness(self.optimal_exp_state) #adds the fitness property to each organism
            self.pop_fitness_values.append(self.organisms[i].fitness)  
            
    
    def mutagenize_population(self, initial_mutations): #randomly mutates every org in the population by a given number of mutations
        for i in range(self.population_size):
            new_strengths = rnd.normal(size = initial_mutations)
            indices = rnd.permutation(range(self.organisms[i].n_interactions))
            for j in range(initial_mutations):
                interaction = self.organisms[i].interactions[indices[j]]
                self.organisms[i].mutate_interaction_strength(interaction[0],interaction[1], new_strengths[j]) 

    def get_rec_load(self):    #measure the fitness hit taken from recombinatioin only, without mutation
        parents1 = self.choose_all_alive() #dead organisms are not subject to rec load measurements
        if len(parents1) < 1: #skip this method if there ar eno living organisms
            return
        parents2 = sorted(parents1, key=lambda k: random.random())
        old_orgs = []
        new_orgs = []
        for i in range(len(parents1)):
            old_orgs.append((parents1[i].fitness + parents2[i].fitness)/2) #make list of avg fitnesses of each parent
        for i in range(len(parents1)):
            offspring = genotype.Genotype.recombine(parents1[i], parents2[i])
            offspring.calculate_fitness(self.optimal_exp_state) #make list of fitness of offspring of each pair
            new_orgs.append(offspring.fitness)
        recload = np.mean(np.divide(new_orgs,old_orgs)) #get mean fitness of new orgs relative to old orgs
        return recload


    def change_pop_opt_exp_state(self, new_opt):
        for org in self.organisms:
            org.change_org_exp_state(new_opt)
        

    def get_deme_hybrid_load(self): 
        hybrid_fitness = []
        l_avg_parent_fitness = []
        r_avg_parent_fitness = []
        r_hybrids = []
        l_hybrids = []
        deme_parents = self.choose_all_alive()
        l_neighbor_parents = self.left_source.choose_all_alive()
        r_neighbor_parents = self.right_source.choose_all_alive()
        while len(r_neighbor_parents) < len(self.organisms):
            r_neighbor_parents.extend(self.right_source.choose_all_alive())
        while len(l_neighbor_parents) < len(self.organisms):
            l_neighbor_parents.extend(self.left_source.choose_all_alive())
        while len(deme_parents) < len(self.organisms): #len(self.organisms):
            deme_parents.extend(self.choose_all_alive())    
        random.shuffle(deme_parents)
        random.shuffle(l_neighbor_parents)
        random.shuffle(r_neighbor_parents)
        for i in range(len(self.organisms)): #range(len(self.organisms)):
            deme_org = deme_parents[i]
            r_org = r_neighbor_parents[i]
            l_org = l_neighbor_parents[i]
            r_avg_parent_fitness.append((r_org.fitness + deme_org.fitness)/2)
            l_avg_parent_fitness.append((l_org.fitness + deme_org.fitness)/2)
            r_hybrid = genotype.Genotype.recombine(r_org, deme_org)
            r_hybrid.calculate_fitness(self.optimal_exp_state)
            r_hybrids.append(r_hybrid.fitness)
            l_hybrid = genotype.Genotype.recombine(l_org, deme_org)
            l_hybrid.calculate_fitness(self.optimal_exp_state)
            l_hybrids.append(l_hybrid.fitness)
        l_recload = np.mean(np.divide(l_hybrids,l_avg_parent_fitness)) 
        r_recload = np.mean(np.divide(r_hybrids,r_avg_parent_fitness)) 
        recload = 1-(l_recload + r_recload)/2
        return recload
            
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~` POPULATION LINEAR #methods that only apply to poulations of linear organisms, after Hartfield et al
class Linear_population(Population): 

    @staticmethod
    def found_clonal_linear_population():
        """
        Make a population by copying a founder genotype into a list population_size times.

        >>> founder = population.Population.generate_founder(5,.2,1)
        >>> g0 = population.Population.found_clonal_population(founder,10)
        >>> g0
        <population.Population at 0x344fb30>
        >>> g0.organisms[2].gene_network
        array([[ 0.48238438,  0.        ,  0.        ,  0.08485553,  0.49141507],
        [-0.79636121,  0.        ,  0.        ,  0.        ,  0.        ],
        [ 0.        ,  0.        ,  0.        ,  0.        ,  0.        ],
        [ 0.        ,  0.        ,  0.        ,  0.        , -0.88783723],
        [ 0.        ,  0.        ,  0.        ,  0.        ,  0.        ]])
        """
        founder = genotype.Linear_genotype.create_linear_genotype()
        founder.get_fitness() 
        new_population = Linear_population()
        new_population.population_size = config.pop_size
        while len(new_population.organisms) < new_population.population_size:
            new_population.organisms.append(copy.deepcopy(founder))
        new_population.pop_fitness_values = [founder.fitness]*config.pop_size
        return new_population

    def get_population_fitness(self): #opt_exp_state = founder.mean_expression_pattern
        """
        Calculate fitness of each genotype, create a vector of each genotype's fitness in the population and make it an attribute of the 
        population
        >>> my_example_population.get_population_fitness()
        >>> my_example_population.pop_fitness_values
           [0.9819314116493095,
           0.9757976889184076]
        """
        self.pop_fitness_values = [] #creates a population instance attribute that is an empty list
        for org in self.organisms:
            org.get_fitness()
            self.pop_fitness_values.append(org.fitness)  
    
    def choose_mixed_all_alive(self):
        sex_orgs = [org for org in self.organisms if org.sex_locus == 1 and org.fitness > 0]
        asex_orgs = [org for org in self.organisms if org.sex_locus == 0 and org.fitness > 0]
        #sex_orgs = [self.organisms[i] for i in range(len(self.organisms)) if self.organisms[i].sex_locus == 1 and self.organisms[i].fitness > 0]
        #asex_orgs = [self.organisms[i] for i in range(len(self.organisms)) if self.organisms[i].sex_locus == 0 and self.organisms[i].fitness > 0]
        return sex_orgs, asex_orgs
    
    def calc_rec_load(self, orgs):
        if len(orgs) == 0:
            return 0
        old_orgs = []
        new_orgs = []
        orgs2 = sorted(orgs, key=lambda k: random.random())
        for parent in xrange(len(orgs)):
            old_orgs.append((orgs[parent].fitness + orgs2[parent].fitness)/2) 
            new_org = orgs[parent].recombine(orgs2[parent])
            new_org.get_fitness()
            new_orgs.append(new_org.fitness)
        #return old_orgs, new_orgs
        recload = np.mean(np.divide(new_orgs,old_orgs))
        return recload
   
    def get_pop_linear_rec_load(self):
        sex_orgs, asex_orgs = self.choose_mixed_all_alive()
        sex_recload = self.calc_rec_load(sex_orgs)
        asex_recload = self.calc_rec_load(asex_orgs)
        return sex_recload, asex_recload
 
    def write_sex_deme_data(self, filename, current_gen_num):
        sex_mean_fit = np.mean([org.fitness for org in self.organisms])
        fit_list = [current_gen_num, sex_mean_fit, "NA", self.proportion_sexual] #need prop sex field       
        filename.write('\t'.join(map(str,fit_list))+'\n')
            

    def write_asex_deme_data(self, filename, current_gen_num):
        asex_mean_fit = np.mean([org.fitness for org in self.organisms])
        fit_list = [current_gen_num, "NA", asex_mean_fit, self.proportion_sexual]        
        filename.write('\t'.join(map(str,fit_list))+'\n')
            
    def write_mixed_deme_data(self, filename, current_gen_num):
        sex_mean_fit = np.mean([org.fitness for org in self.organisms if org.sex_locus ==1])
        asex_mean_fit = np.mean([org.fitness for org in self.organisms if org.sex_locus == 0])
        fit_list = [current_gen_num, sex_mean_fit, asex_mean_fit, self.proportion_sexual]        
        filename.write('\t'.join(map(str,fit_list))+'\n')   
                   
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  Metapopulation  Class #methods which apply to both matrix_populations and linear_populations
class Metapopulation(object):
    def __init__(self): 
        self.deme_list = []
        self.no_wrap_migration = False 

    
    def choose_migrants(self, n_migrants): #chooses number of migrants based on migration rate and sends them to neighboring demes. each deme has a pool for migrants, either left or right        
        for node in range(self.n_demes):
            deme = self.deme_list[node]
            random.shuffle(deme.organisms) #puts all orgs in a random order
            destinations = ("right_migrants", "left_migrants")
            for x in destinations: #left or right
                migrants = [] #make sure there are no migrants lined up already
                mig_num = min(rnd.poisson(n_migrants/2.0), len(deme.organisms)) #chooses # of migrants from poisson dist based on deme size*mig_rate, will not choose more than are in pop              
                for migrant in range(mig_num): #chooses outgoing organisms to either side
                    if x == "right_migrants":
                        deme.right_migrants.append(deme.organisms.pop()) #remove org from population, puts in migrant pool. they do not join new popoulation yet
                    elif x == "left_migrants":               
                        deme.left_migrants.append(deme.organisms.pop())               
                        
    def choose_no_wrap_migrants(self, n_migrants):
        first = {'deme':self.deme_list[0], 'migrants': self.deme_list[0].right_migrants}
        last = {'deme':self.deme_list[self.n_demes-1], 'migrants': self.deme_list[self.n_demes-1].left_migrants}
        edge_demes = [first,last]
        for edge in edge_demes:
            random.shuffle(edge['deme'].organisms)        
            mig_num = 1 #mig_num = min(rnd.poisson(n_migrants/2.0), len(edge.organisms))
            for migrant in range(mig_num): #chooses outgoing organisms to either side
                edge['migrants'].append(edge['deme'].organisms.pop()) #remove org from population, puts in migrant pool. they do not join new popoulation yet           
        for deme in range(1, self.n_demes-1):
            random.shuffle(self.deme_list[deme].organisms) #puts all orgs in a random order
            destinations = (self.deme_list[deme].right_migrants, self.deme_list[deme].left_migrants)
            for x in destinations: #left or right
                mig_num = min(rnd.poisson(n_migrants/2.0), len(self.deme_list[deme].organisms)) #chooses #migrants from poisson dist based on nd*mig_rate, won't choose more than are in pop              
                for migrant in range(mig_num): #chooses outgoing organisms to either side
                    x.append(self.deme_list[deme].organisms.pop())



    def get_hybrid_load(self): #metapop
        for deme in self.deme_list:
            hybrid_fitness = []
            l_avg_parent_fitness = []
            r_avg_parent_fitness = []
            r_hybrids = []
            l_hybrids = []
            deme_parents = deme.choose_all_alive()
            l_neighbor_parents = deme.left_source.choose_all_alive()
            r_neighbor_parents = deme.right_source.choose_all_alive()
            for i in range(1000):
                deme_org = random.choice(deme_parents)
                r_org = random.choice(r_neighbor_parents)
                l_org = random.choice(l_neighbor_parents)
                r_avg_parent_fitness.append((r_org.fitness + deme_org.fitness)/2)
                l_avg_parent_fitness.append((l_org.fitness + deme_org.fitness)/2)
                r_hybrid = genotype.Genotype.recombine(r_org, deme_org)
                r_hybrid.calculate_fitness(deme.optimal_exp_state)
                r_hybrids.append(r_hybrid.fitness)
                l_hybrid = genotype.Genotype.recombine(l_org, deme_org)
                l_hybrid.calculate_fitness(deme.optimal_exp_state)
                l_hybrids.append(l_hybrid.fitness)
            l_recload = np.mean(np.divide(l_hybrids,l_avg_parent_fitness)) #should these recloads be a single list? prob
            r_recload = np.mean(np.divide(r_hybrids,r_avg_parent_fitness)) #if they are a single list, what about old parents? 
            recload = (l_recload + r_recload)/2
            return recload
    
    def migrate(self, n_migrants): 
        self.choose_migrants(n_migrants) #each deme has migrants in the pool of orgs that will join it but they have not yet been added
        for i in range(self.n_demes): 
            deme = self.deme_list[i]
            deme.organisms += deme.left_source.right_migrants #adds emigrants to current population
            del(deme.left_source.right_migrants[:]) #empties the pools of migrants
            deme.organisms += deme.right_source.left_migrants
            del(deme.right_source.left_migrants[:])

    def migrate_no_wrap(self, n_migrants): 
        self.choose_no_wrap_migrants(n_migrants) #each deme has migrants in the pool of orgs that will join it but they have not yet been added
        for i in range(self.n_demes): 
            deme = self.deme_list[i]
            deme.organisms += deme.left_source.right_migrants #adds emigrants to current population
            del(deme.left_source.right_migrants[:]) #empties the pools of migrants
            deme.organisms += deme.right_source.left_migrants
            del(deme.right_source.left_migrants[:])


    def mutate_asexual(self, n_demes,pop_size): #chooses random organisms from metapopulation and mutates it to asexual
        self.deme_list[random.randrange(n_demes)].organisms[random.randrange(pop_size)].sex_locus = 0.0
 
    def mutate_sexual(self, n_demes,pop_size): #chooses random organisms from metapopulation and mutates it to asexual
        self.deme_list[random.randrange(n_demes)].organisms[random.randrange(pop_size)].sex_locus = 1.0
 
           
    def get_metapop_prop_sexual(self): #get proportion sexual of all organisms in pop  
        for deme in self.deme_list:
            deme.get_proportion_sexual()    
        metasex = [self.deme_list[i].proportion_sexual for i in range(len(self.deme_list)) if self.deme_list[i].proportion_sexual is not None]
        self.prop_sex = np.mean(metasex)

    def set_metapop_prop_asexual(self):
        num_asex = config.meta_pop_size * config.invasion_proportion     
        asexuals = 0
        for deme in self.deme_list:
            for org in deme.organisms:
                if asexuals < num_asex:
                    org.sex_locus = 0
                    asexuals += 1
                else:
                    break
    def assign_meta_invasion_properties(self):
        for deme in self.deme_list:
            for org in deme.organisms:
                org.assign_invasion_properties()
                            
    def set_metapop_prop_sexual(self):
        num_sex = config.meta_pop_size * config.invasion_proportion     
        sexuals = 0
        for deme in self.deme_list:
            for org in deme.organisms:
                if sexuals < num_sex:
                    org.sex_locus = 1
                    sexuals += 1
                else:
                    break
                    
    def flip_one_sex_locus(self):
        deme = self.deme_list[random.randrange(self.n_demes)]
        org = deme.organisms[random.randrange(len(deme.organisms))]
        org.sex_locus = (org.sex_locus+1)%2    
                                
    def measure_sex_fixation(self, pid, gen_post_invasion): #checks prop sexual
        self.get_metapop_prop_sexual()
        if self.prop_sex == 0 or self.prop_sex == 1: #if sex or asex has fixed,
            return True #returns stop or continue signal
            #self.record_metapop_invasion_data(self, pid, gen_since_invasion)
        else:
            return False

    def get_metapop_avg_sex_asex_fitness(self, fit_file, current_gen):
        for deme in self.deme_list:
            sex_orgs = []
            asex_orgs = []
            sex_orgs.extend([org.fitness for org in deme.organisms if org.sex_locus == 1.0])
            asex_orgs.extend([org.fitness for org in deme.organisms if org.sex_locus == 0.0])
            fit_file.write(str(np.mean(sex_orgs))+"\t"+ str(np.mean(asex_orgs))+"\t" + str(current_gen) + "\n") 

    def record_metapop_invasion_data(self, pid, gen_since_invasion): 
        filename = open("Invasion" + "_" + str(pid) + "_Gen" + str(gen_since_invasion) + "_" + str(self.winner) + ".txt", "w")
        self.write_file(filename, n_demes, optimal_exp_state, replicates, number_of_mutations, gen_since_invasion)
       
    def record_metapop_burnin_data(self, foundernum, replicates, number_of_mutations, generation,optimal_exp_state, n_demes, pid): #only used for burn-in pops for invasions, not for basic ev. expts.
        filename = open("Founder" + "_" + str(foundernum) + "_Gen" + str(generation) + "_burn_in_" + str(pid) + ".txt", "w")
        rob_header = []
        self.write_file(filename, n_demes, optimal_exp_state, replicates, number_of_mutations, generation)  
    
    def _pickle_metapopulation(self, foundernum, current_gen, pid, rep):
        file = open("Metapopulation_Founder_" + str(foundernum)+ "_" + str(current_gen)+ "_" + str(pid) + "_" + str(rep) + ".pickle", "w")
        cPickle.dump(self, file, cPickle.HIGHEST_PROTOCOL) #choosing cPickle makes it faster, designating the priority makes the file about 1/3 the size
        file.close()

    @staticmethod
    def _unpickle_metapopulation(filename): #pass in the metapopulation pickle
        file = open(str(filename), "r") 
        pop = cPickle.load(file) #cPickle for speed
        file.close()
        return pop      
 
    @staticmethod
    def open_files(pid): #opens all files, write headers to them, and returns them in dict form
        population_datafile = open("pop_data_"+str(pid) + ".txt","w")
        org_header = ["Gen", "Fitness","Sex", "Deme", "Pert", "Rob1", "Pr_Ben", "Pr_Neutral", "Pr_Lethal"]
        for i in range(config.num_loci):
            org_header.append("Neut"+str(i) + "\t")    
        #full_header = org_header + neut_header
        population_datafile.write('\t'.join(map(str,org_header))+'\n')
        deme_varfile = open("deme_var_"+str(pid)+".txt","w")
        deme_varfile.write("Gen" + "\t" "Deme" + "\t" + "Het" + "\n")
        metapop_varfile = open("metapop_var_"+str(pid)+".txt","w")
        metapop_varfile.write("Gen" + "\t" + "Het" + "\n")
        recload_file = open("rec_load_" + str(pid) + ".txt","w")
        recload_file.write("Generation"+ "\t" + "RecLoad" + "\n")
        file_keys = ['population_datafile', 'deme_varfile', 'metapop_varfile', 'recload_file']
        file_vals = [population_datafile, deme_varfile, metapop_varfile, recload_file]
        all_files = dict(zip(file_keys, file_vals))
        return all_files
    
    @staticmethod
    def open_invasion_files(pid, rep, timenow): #opens all files, write headers to them, and returns them in dict form
        population_datafile = open("pop_data_"+str(pid) + "_" + str(rep) + "_" + str(timenow) + ".txt","a")
        population_datafile.write("Gen"+"\t" + "Fitness" + "\t" + "Sex" + "\t" + "Deme" + "\t" + "Pert" + "\t" + "RawRob" + "\t" + "Rob1" + "\t" + "Prop_sexual" + "\n")
        deme_varfile = open("deme_var_"+str(pid) + "_" + str(rep) + "_" + str(timenow) + ".txt","a")
        deme_varfile.write("Gen" + "\t" "Deme" + "\t" + "Het" + "\n")
        metapop_varfile = open("metapop_var_"+str(pid) + "_" + str(rep) + "_" + str(timenow) + ".txt","a")
        metapop_varfile.write("Gen" + "\t" + "Het" + "\n")
        recload_file = open("rec_load_" + str(pid) + "_" + str(rep) + "_" +  str(timenow) + ".txt","a")
        recload_file.write("Generation"+ "\t" + "RecLoad" + "\n")
        file_keys = ['population_datafile', 'deme_varfile', 'metapop_varfile', 'recload_file']
        file_vals = [population_datafile, deme_varfile, metapop_varfile, recload_file]
        all_files = dict(zip(file_keys, file_vals))
        return all_files
    
    @staticmethod
    def reopen_files():
        for file in os.listdir('.'): #match to specific files
            if fnmatch.fnmatch(file, 'pop_data*'):
                population_datafile = open(str(file),"a")
            if fnmatch.fnmatch(file, 'deme_var*'):
                deme_varfile = open(str(file),"a")
            if fnmatch.fnmatch(file, 'metapop_var*'):
                metapop_varfile = open(str(file),"a")
            if fnmatch.fnmatch(file, 'rec_load*'):
                recload_file = open(str(file),"a")
        file_keys = ['population_datafile', 'deme_varfile', 'metapop_varfile', 'recload_file']
        file_vals = [population_datafile, deme_varfile, metapop_varfile, recload_file]
        all_files = dict(zip(file_keys, file_vals))
        return all_files
       
    @staticmethod
    def close_files(all_files): #pass in the dict that includes population_datafile, deme_varfile, metapop_varfile, recload_file
        all_files['population_datafile'].close()
        all_files['deme_varfile'].close()
        all_files['metapop_varfile'].close()
        all_files['recload_file'].close()
      
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  Metapop linear ------------------    Methods which only to metapopulations composed of linear populations
class Metapopulation_linear(Metapopulation):
    
    @staticmethod
    def create_linear_metapopulation():
        metapop = Metapopulation_linear()
        founding_pop = Linear_population.found_clonal_linear_population() #found a clonal population based on the given founder
        metapop.n_demes = config.n_demes
        for i in xrange(config.n_demes):
            metapop.deme_list.append(copy.deepcopy(founding_pop)) #demes are clones of founding clonal population
        for i in xrange(config.n_demes):
            metapop.deme_list[i].right_source = metapop.deme_list[(i + 1) % metapop.n_demes] #give each deme a property of which demes it's proximal to
            metapop.deme_list[i].left_source = metapop.deme_list[(i -1) % metapop.n_demes]
        return metapop

    def write_file(self, filename, n_demes, generation):
        header = ["Gen","Org", "Sex Fitness", "Asex_Fitness","Sex", "Deme"]     
        filename.write('\t'.join(map(str,header))+'\n')
        self.write_org_data(filename, n_demes,generation)
        all_files['recload_file'].write(str(current_gen_num) + '\t' + str(1-(np.mean(deme_recs)))+'\n')   
    
        
    def write_org_data(self, filename, current_gen_num):
        for deme in range(config.n_demes):
            for org in range(len(self.deme_list[deme].organisms)):
                #org_mean = np.mean(org.fitness for org in self.deme_list[deme].organisms)
                current_org = self.deme_list[deme].organisms[org]
                fit_list = [current_gen_num, org, current_org.fitness, current_org.sex_locus, deme]        
                filename.write('\t'.join(map(str,fit_list))+'\n')
        
    
    def write_org_data_summary(self, filename, current_gen_num): #unfinished method
        for deme in self.deme_list:
            deme.get_proportion_sexual()
            if not 1-deme.proportion_sexual: ##if proportion_sexuals = 1, 1-prop sex retruns false and will write sex data
                deme.write_sex_deme_data(filename, current_gen_num)
            if not deme.proportion_sexual: #if there are 0 sexuals, write asex
                deme.write_asex_deme_data(filename, current_gen_num)
            else:
                deme.write_mixed_deme_data(filename, current_gen_num)
               
        
    def get_meta_rec_load(self, recload_file, current_gen_num): 
        sex_deme_recs = []
        asex_deme_recs = []
        for deme in self.deme_list:
            sex_recload, asex_recload = deme.get_pop_linear_rec_load()
            if sex_recload:
                if sex_recload > 1:
                    sex_recload = 0
                sex_deme_recs.append(sex_recload)
            else:
                next
            if asex_recload:
                if sex_recload > 1:
                    sex_recload = 0        
                asex_deme_recs.append(asex_recload)
            else:
                next
        if sex_deme_recs and asex_deme_recs:
            recload_file.write(str(current_gen_num) + '\t' + str(1-(np.mean(sex_deme_recs)))+ '\t' + str(1-(np.mean(asex_deme_recs)))+ '\n')
        elif sex_deme_recs:
            recload_file.write(str(current_gen_num) + '\t' + str(1-(np.mean(sex_deme_recs)))+ '\t' + str("NA")+ '\n')
        elif asex_deme_recs:
            recload_file.write(str(current_gen_num) + '\t' + str("NA")+ '\t' + str(1-(np.mean(asex_deme_recs)))+ '\n')
        else:
            recload_file.write(str(current_gen_num) + '\t' + str("NA")+ '\t' + str("NA")+ '\n')
            
    @staticmethod
    def close_files(pop_file, rec_file): #pass in the dict that includes population_datafile, deme_varfile, metapop_varfile, recload_file
        pop_file.close()
        rec_file.close()      



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Metapop matrix-------------- Methods which only apply to metapopulations composed of gene network populations
class Metapopulation_matrix(Metapopulation):
    def __init__(self):
        self.perturbed = "false"
        self.deme_list = []

    def generate_new_exp_state(self, optimum): 
        self.perturbed = "true"
        myrandoms = random.sample(xrange(len(optimum)), config.number_of_environ_changes)
        for i in myrandoms:
            optimum[i] = optimum[i]*-1
        return optimum

    def change_metapop_opt_exp_state(self, optimum):
        optimum = self.generate_new_exp_state(optimum)
        for deme in self.deme_list:
            deme.change_pop_opt_exp_state(optimum) #this changes optimum and initial_exp_state for all orgs in the deme
        return optimum

    def create_demes_from_founder(self,founder):
        self.n_demes = config.n_demes
        founding_pop = Matrix_population.found_clonal_matrix_population(founder) #found a clonal population based on the given founder
        for i in xrange(self.n_demes):
            self.deme_list.append(copy.deepcopy(founding_pop)) #demes are clones of founding clonal population
        for i in xrange(self.n_demes):
            self.deme_list[i].right_source = self.deme_list[(i + 1) % self.n_demes] #give each deme a property of which demes it's proximal to
            self.deme_list[i].left_source = self.deme_list[(i -1) % self.n_demes]

                
    def calc_metapop_gen_var(self): #pairwise comparisons for organisms in the metapopulation, across all demes
        populated_demes = [self.deme_list[i] for i in range (len(self.deme_list)) if len(self.deme_list[i].organisms) > 0] #run if the deme is not empty
        het_sum = 0
        het_list = []
        rand_deme_vals = rnd.randint(len(populated_demes), size = 2*config.n_meta_draws) #picks random demes to compare organisms from
        for i in xrange(0, 2*config.n_meta_draws, 2):
            deme1 = populated_demes[rand_deme_vals[i]]
            org1 = deme1.organisms[random.randrange(len(deme1.organisms))] #picks random org within the deme
            deme2 = populated_demes[rand_deme_vals[i+1]]  
            #while deme1 == deme2:
            #    deme2 = populated_demes[rand_deme_vals[i+1]]            
            org2 = deme2.organisms[random.randrange(len(deme2.organisms))] #picks random org within the deme
            het_list.append(org1.calc_genetic_variation(org2)) #get genetic variation between the two organisms & add it to a list
        het_avg = np.mean(het_list)
        return het_avg

    def write_pop_data(self, all_files, optimal_exp_state, current_gen_num): #takes all_files dict as an argument, calculates robustness & writes to file    
        for deme in range(len(self.deme_list)):
            for org in self.deme_list[deme].organisms:
                if not org.fitness:
                    org.mean_mutant_fitness = "NA"
                    org.frequency_beneficial = "NA"
                    org.frequency_neutral = "NA"
                    org.frequency_lethal = "NA"
                else:
                    org.calculate_mutant_fitness_distribution(optimal_exp_state) 
                    rel_robustness = org.multiple_mutation_robustness[config.number_of_mutations-1]/org.fitness #uses final value of robustness test                    
                fit_list = [current_gen_num, org.fitness, org.sex_locus, deme, self.perturbed, org.mean_mutant_fitness, org.frequency_beneficial, org.frequency_neutral, org.frequency_lethal] + org.neutral_locus
                all_files['population_datafile'].write('\t'.join(map(str,fit_list))+'\n')
                
    def write_invasion_pop_data(self, all_files, optimal_exp_state, current_gen_num): #takes all_files dict as an argument, calculates robustness & writes to file    
        for deme in range(len(self.deme_list)):
            for org in self.deme_list[deme].organisms:
                if not org.fitness:
                    rel_robustness = "NA"
                else:
                    org.calculate_robustness(optimal_exp_state)
                    rel_robustness = org.multiple_mutation_robustness[config.number_of_mutations-1]/org.fitness #uses final value of robustness test
                fit_list = [current_gen_num, org.fitness, org.sex_locus, deme, self.perturbed, str(org.multiple_mutation_robustness[config.number_of_mutations-1]), str(rel_robustness), str(self.prop_sex)] + org.neutral_locus          
                all_files['population_datafile'].write('\t'.join(map(str,fit_list))+'\n')

     
    def write_all_invasion_data(self,all_files, optimum, current_gen_num):
        self.write_invasion_pop_data(all_files, optimum, current_gen_num)
        self.record_var(all_files, current_gen_num)
        self.get_meta_rec_load(current_gen_num, all_files)
        
                    
    def write_all_data(self,all_files, optimum, current_gen_num):
        self.write_pop_data(all_files, optimum, current_gen_num)
        self.record_var(all_files, current_gen_num)
        self.get_meta_rec_load(current_gen_num, all_files)
	all_files['metapop_varfile'].flush()
	all_files['deme_varfile'].flush()
	all_files['recload_file'].flush()
    
    def record_var(self, all_files, current_gen_num): #gets genetic variation for deme and metapop and writes them to the file
        for i in xrange(self.n_demes):
            het = self.deme_list[i].calc_pop_gen_var()
            all_files['deme_varfile'].write(str(current_gen_num) + '\t' + str(i) + '\t' + str(het) + '\n') #str het needs tobe retruned as a list. how was it done in old rob? 
        metahet = self.calc_metapop_gen_var()
        all_files['metapop_varfile'].write(str(current_gen_num) + '\t' + str(metahet) +  '\n')  #str metahet needs to be a list
        
    def get_meta_rec_load(self, current_gen_num, all_files): #gets rec load for each deme within the metapop
        deme_recs = []
        for deme in self.deme_list:
            deme_rec_val = deme.get_rec_load()           
            if deme_rec_val: # check to see if the value is not None, ie if there were living orgs
                if deme_rec_val > 1:
                    deme_rec_val = 0
                deme_recs.append(deme_rec_val) 
        all_files['recload_file'].write(str(current_gen_num) + '\t' + str(1-(np.mean(deme_recs)))+'\n')

	#def get_metapop_avg_sex_asex_fitness(self):
	#	sex_orgs = []
   	#	asex_orgs = []
	#	for deme in self.deme_list:
	#		sex_orgs.extend([org.fitness for org in deme.organisms if org.sex_locus == 1.0])
	#		asex_orgs.extend([org.fitness for org in deme.organisms if org.sex_locus == 0.0])
	#	return np.mean(sex_orgs). np.mean(asex_orgs)


       
 
 
            
    
        
