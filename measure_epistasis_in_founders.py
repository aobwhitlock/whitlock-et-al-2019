# -*- coding: utf-8 -*-
import population
import os
import genotype
import config
import fnmatch
import copy
import random
import numpy as np

#set parameters from config file
pop_size = config.pop_size
n_demes = config.n_demes
n_migrants = config.n_migrants
foundernum = config.foundernum
cost = config.cost_of_sex 


#open file for data storage and write header
pid = os.getpid()
filename = "epistasis_in_founders_"+str(pid)+".txt" 
population_datafile = open(filename,"a")
population_datafile.write("Founder"+"\t"+"Gen"+"\t"+"WT_No"+"\t"+"Pair_No"+"\t"+"WildType_Fitness"+"\t"+"Mutant1_Fitness"+"\t"+"Mutant2_Fitness"+"\t"+"Mutant12_Fitness"+"\n")
#population_datafile.close()

#unpickle founders 
founding_pop = population.Matrix_population._unpickle(3)
i=0
for org in founding_pop.organisms:
    i=i+1
    #assign or calculate all properties
    org.assign_properties()
    org.develop()
    optimum = org.mean_expression_pattern
    org.calculate_fitness(optimum)

    #for each founder, generate 100 mutation pairs and measure fitness individually and in combination.
    wt = 0
    if org.fitness > 0:
        wt = wt + 1
        for rep in range(10000):
            mutant1 = copy.deepcopy(org)
            mutant2 = copy.deepcopy(org)
            mutant12 = copy.deepcopy(org)
            new_strengths = np.random.normal(size = 2)
            mutations = random.sample(range(len(org.interactions)),2)
            mutant1.mutate_interaction_strength(org.interactions[mutations[0]][0], org.interactions[mutations[0]][1], new_strengths[0])
            mutant2.mutate_interaction_strength(org.interactions[mutations[1]][0], org.interactions[mutations[1]][1], new_strengths[1])
            mutant12.mutate_interaction_strength(org.interactions[mutations[0]][0], org.interactions[mutations[0]][1], new_strengths[0])
            mutant12.mutate_interaction_strength(org.interactions[mutations[1]][0], org.interactions[mutations[1]][1], new_strengths[1])
            mutant1.develop()
            mutant1.calculate_fitness(optimum)
            mutant2.develop()
            mutant2.calculate_fitness(optimum)
            mutant12.develop()
            mutant12.calculate_fitness(optimum)
                    
            #population_datafile = open(filename,"a")
            population_datafile.write(str(i) + "\t" + str(0) + "\t" + str(wt) + "\t" + str(rep) + "\t" + str(org.fitness) + "\t" + str(mutant1.fitness) + "\t" + str(mutant2.fitness) + "\t" + str(mutant12.fitness) + "\n")

population_datafile.close()
    

