import population
import genotype
import numpy as np
import fnmatch
import os

for file in os.listdir('.'): #match to specific files
	if fnmatch.fnmatch(file, 'Metapopulation*10000*'):
		pop = population.Metapopulation._unpickle_metapopulation(str(file))
    
        
optimal_exp_state = pop.deme_list[0].optimal_exp_state
migfile = open("mig_load_10000.txt", "w")
header = ["offspring_number","migrant_deme_number","resident_deme_number", "migrant_fitness", "resident_fitness", "offspring_fitness", "offspring_relative_fitness"] 
migfile.write('\t'.join(map(str,header))+'\n')

for deme in pop.deme_list:
    deme.reproduce_pop_by_recombination_probability()
    deme.get_population_fitness()

pop.n_demes = len(pop.deme_list)
for deme_num in range(len(pop.deme_list)):
    deme = pop.deme_list[deme_num]

    migrant_choices = deme.choose_next_generation_individuals() #migrant parents are all individuals within the deme regardless of fitness and viability
    migrant_parents = []
    for choice in migrant_choices:
        migrant_parents.append(deme.organisms[choice])
    directions = [((deme_num -1) % pop.n_demes), ((deme_num +1) % pop.n_demes)] #assign L and R demes
   
    for direction in directions: #for each deme, both left and right directions are tested
        dest_deme = pop.deme_list[direction]
        dest_fit = np.mean(dest_deme.pop_fitness_values)
        dest_choices = []
        dest_parents = []
        while len(dest_choices) < len(deme.organisms): #did this for flexibility if we want to change the number of recombinations, ie while len < 100:
            dest_choices.extend(dest_deme.choose_next_generation_individuals()) #2nd parent chosen by fitness
        for choice in dest_choices:
            dest_parents.append(dest_deme.organisms[choice])
   
        for recombination in range(len(deme.organisms)): #did this for flexibility if we want to change the number of recombinations, ie while len < 100:
            offspring = genotype.Genotype.recombine(migrant_parents[recombination], dest_parents[recombination])
            offspring.calculate_fitness(optimal_exp_state)
            
            #assign vals for write legibility
            mig_fit = migrant_parents[recombination].fitness
            parent2_fit = dest_parents[recombination].fitness
            offspring_relative_fitness = 2 * offspring.fitness / (mig_fit + parent2_fit)
            
            results = [recombination, deme_num, direction, mig_fit, parent2_fit, offspring.fitness, offspring_relative_fitness] 
            migfile.write('\t'.join(map(str,results))+'\n')
            
migfile.close()
            
