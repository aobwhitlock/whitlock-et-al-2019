import population
import os
import genotype
import config
import fnmatch
import copy
import time

#pop_size = config.meta_pop_size/config.n_demes #write this with shell script so that this calculation will be performed after variables are assigned
n_migrants = config.migration_rate*config.pop_size 
#open files for data storage
pid = os.getpid()
for file in os.listdir('.'): #match to specific files
	if fnmatch.fnmatch(file, 'Metapopulation*'):
		metapop_founder = population.Metapopulation._unpickle_metapopulation(str(file))
		optimum = metapop_founder.deme_list[0].optimal_exp_state




metapop_founder.assign_meta_invasion_properties()
timenow = time.time()
for rep in range(config.rep):
	#all_files = population.Metapopulation.open_invasion_files(pid, rep, timenow) 
	prop_file = open("prop_sex_"+str(pid)+ "_" + str(rep) + "_" +  str(timenow) + ".txt","a") 
	prop_file.write("Founder"+"\t"+"Rep"+"\t"+"Gen"+"\t" + "Sex Frequency" + "\n")
	fit_file = open("rel_fit_file_"+str(pid)+ "_" + str(rep) + "_" + str(timenow) + ".txt","a") 
	fit_file.write("Sex_fit"+"\t"+"Asex_fit"+"\t"+"Gen" + "\n") 
	metapop = copy.deepcopy(metapop_founder)
	metapop.flip_one_sex_locus()


	#begin evolution
	current_gen_num = 0
	while not metapop.measure_sex_fixation(pid, current_gen_num):
	    prop_file.write(str(config.foundernum) + "\t" + str(rep) + "\t" + str(current_gen_num) + "\t" + str(metapop.prop_sex) + "\n")
	    metapop.get_metapop_avg_sex_asex_fitness(fit_file, current_gen_num)
	    for i in xrange(config.n_demes):
	        if not len(metapop.deme_list[i].organisms):
	            next
	        else:    
		        metapop.deme_list[i].reproduce_pop_by_recombination_probability()
		        metapop.deme_list[i].get_population_fitness()
	    metapop.migrate(n_migrants)
	    if current_gen_num == 4000:
		    metapop._pickle_metapopulation(config.foundernum, current_gen_num, pid, rep)
		    #metapop.write_all_invasion_data(all_files, optimum, current_gen_num)
		    break
	    current_gen_num += 1
	    
	print current_gen_num
	print "fixed"
	print metapop.prop_sex
	prop_file.write(str(config.foundernum) + "\t" + str(rep) + "\t" + str(current_gen_num) + "\t" + str(metapop.prop_sex) + "\n")
        prop_file.close()
	fit_file.close()
	#metapop.write_all_invasion_data(all_files, optimum, current_gen_num)
	#population.Metapopulation.close_files(all_files)
	#metapop._pickle_metapopulation(config.foundernum, current_gen_num, pid, rep)
